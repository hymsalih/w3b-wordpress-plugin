<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 6/25/2019
 * Time: 12:06 PM
 */

//include "WP-ROOT-PATH/wp-config.php";
class w3bStoreClass
{
    private $access_token;
    private $apiUrl = "http://69.60.124.194/api";
    private $w3bStoreApp = "http://69.60.124.194/wordpress-auth";

    function __construct()
    {
        global $wpdb;
        $auth = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "w3bstore_api");
        if (!empty($auth)) {
            $this->access_token = $auth[0]->access_token;
        }
    }

    function w3bstore_user_revoke_token()
    {
        global $wpdb;
        $table = $wpdb->prefix . "w3bstore_api";
        $auth = $wpdb->get_results("SELECT * FROM $table ORDER BY ID DESC LIMIT 1");
        if ($auth) {

        }
    }


    function w3bstore_user_stored_token()
    {
        global $wpdb;
        $table = $wpdb->prefix . "w3bstore_api";
        $auth = $wpdb->get_results("SELECT * FROM $table ORDER BY ID DESC LIMIT 1");
        return (!empty($auth) ? $auth[0] : '');
    }

    function w3bstore_uninstall_tables()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "w3bstore_api";
        $wpdb->query("DROP TABLE IF EXISTS " . $table_name);
    }

    function w3bstoreAuthenticationTokenSave($data)
    {
        global $wpdb;
        $table = $wpdb->prefix . "w3bstore_api";
        $auth = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "w3bstore_api");
        $access_token = $data['access_token'];
        $token_type = $data['token_type'];
        $expired_at = $data['expires_at'];
        if (empty($auth)) {
            $sql = "INSERT INTO $table (access_token, token_type, created_at, expires_at) VALUES ('$access_token', '$token_type', CURRENT_TIMESTAMP,'$expired_at')";
            $wpdb->query($sql);
        } else {
            $sql = "UPDATE $table  SET access_token = '$access_token',expires_at = '$expired_at' WHERE id = '" . $auth[0]->id . "'";
            $wpdb->query($sql);
        }
    }

    function w3bstoreLogin()
    {
        $auth = $this->w3bstore_user_stored_token();
        if (!empty($auth)) {
            return $this->w3bStoreCurlApiPost($postDate = array(), '/login');
        }
    }

    function w3bstoreActiveMenu()
    {
        return $this->w3bStoreCurlApiPost($postDate = array(), '/menu');
    }

    function w3bstore_install_tables()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "w3bstore_api";
        $charset_collate = $wpdb->get_charset_collate();
        $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  access_token text NULL,
  token_type varchar(55) DEFAULT '' NOT NULL,
  created_at  varchar(55) DEFAULT '' NOT NULL,
  expires_at varchar(55) DEFAULT '' NOT NULL,
  PRIMARY KEY  (id)
) $charset_collate;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        $success = empty($wpdb->last_error);
        return $success;
    }

    function w3bStoreCurlGet($endpoint)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8000",
            CURLOPT_URL => $this->apiUrl . $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $this->access_token,
                "Accept: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return ['status' => 'error', 'msg' => $err];
        } else {
            return $response;
        }
    }


    function w3bStoreCurlApiPost($postData, $endpoint)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->apiUrl . $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $this->access_token,
                "Accept: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return ['status' => 'error', 'msg' => $err];
        } else {
            return $response;
        }
    }

    function w3bStoreCurlPost($postData, $endpoint)
    {
        $query = http_build_query($postData);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->w3bStoreApp,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $query,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return ['status' => 'error', 'msg' => $err];
        } else {
            return $response;
        }
    }
}
