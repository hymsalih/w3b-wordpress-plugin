<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 6/23/2019
 * Time: 10:53 PM
 */
//require(BASE_PATH . 'wp-load.php');
$action = (!empty($_POST['action']) ? $_POST['action'] : '');

switch ($action) {
    case "auth":
        auth();
        break;
    case "auth-revoke":
        authRevoke();
        break;
    default:
        break;
}

function authRevoke()
{
    global $wpdb;
    $obj = new w3bStoreClass();
    $auth = $wpdb->get_results("SELECT * FROM  " . $wpdb->prefix . "w3bstore_api  ORDER BY id DESC LIMIT 1");
    if ($auth) {
        $response = $obj->w3bStoreCurlApiPost(['token' => $auth[0]->access_token, 'client' => 'wordpress-app'], "/auth/revoke");
        $response = json_decode($response, true);
        $location = $_SERVER['HTTP_REFERER'];
//        $location = "/admin.php?page=setup";
        if (!empty($response) && $response['status'] == "success") {
            $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "w3bstore_api");
            $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "w3bstore_menus");
            wp_safe_redirect($location . "&status=success&msg=Authentication token revoked");
        } else if (!empty($response) && $response['status'] == "error" && $response['message'] == "You pass invalid token") {
            $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "w3bstore_api");
            $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "w3bstore_menus");
            wp_safe_redirect($location . " & status=success&msg=Authentication token revoked");
        } else {
            wp_safe_redirect($location . " & status=error&msg=Authentication token not revoked");
        }
    }
}

function auth()
{
    global $wpdb;
    $obj = new w3bStoreClass();
    $email = $_POST['email'];
    $password = $_POST['password'];
    $account = (isset($_POST['new_account']) ? 1 : 0);
    $location = $_SERVER['HTTP_REFERER'];
    if (isset($account) && $account == 1) {
        //new user
        $url = "https://w3bstore.com/market.php/symfony/createstore/adid/5000/email/$email/new_password1/$password";
        $url = "https://w3bstore.com/market.php/symfony/createstore/adid/5000/email/$email/new_password1/$password";
        header("Location: $url");
        exit;
    } else {
        $url = "https://w3bstore.com/market.php/security/login/email/$email/new_password1/$password";
        header("Location: $url");
        exit;
        //existing user
        $response = $obj->w3bStoreCurlPost(['email' => $email, 'password' => $password, 'client' => 'wordpress-app'], "wordpress-auth");
        $response = json_decode($response, true);
        if (!empty($response) && $response['status'] == "success") {
            $obj->w3bstoreAuthenticationTokenSave($response);
            $table = $wpdb->prefix . "w3bstore_menus";
            $sql = "INSERT INTO $table (bar_name, status, icon) VALUES ('Dashboard', '1', 'dashboard')";
            $wpdb->query($sql);
//            $obj = new w3bStoreClass();
//            $responseActiveMenus = $obj->w3bStoreCurlApiPost($postDate = array(), '/menu');
//            $responseActiveMenus = json_decode($responseActiveMenus, true);
//            if ($responseActiveMenus['status'] == "success") {
//                $wpdb->query("DELETE FROM $table");
//                $menus = $responseActiveMenus['menus'];
//                foreach ($menus as $meun) {
//                    $sql = "INSERT INTO $table (bar_name, status, icon) VALUES ('" . $meun['bar_name'] . "', '" . $meun['status'] . "', '" . $meun['icon'] . "')";
//                    $wpdb->query($sql);
//                }
//            }
            wp_safe_redirect($location);
        } else {
            wp_safe_redirect($location);
        }
    }

}

function w3bstore_success_notice($success_message)
{
    echo '<div class="notice success-info is-dismissible">
                         <p style="text-align: left">' . $success_message . '</p>
         </div>';
}

function w3bstore_info_notice($success_message)
{
    echo '<div class="notice notice-info is-dismissible">
                         <p style="text-align: left">' . $success_message . '</p>
         </div>';
}

function w3bstore_warning_notice()
{
    global $success;
    if (isset($success)) {
        echo '<div class="notice notice-error is-dismissible">
             <p style="text-align: left">' . $success . '</p>
         </div>';
    };
}

function w3bstore_error_notice()
{
    global $errors;
    if (isset($errors)) {
        echo '<div class="notice notice-error is-dismissible">
             <p style="text-align: left">' . $errors . '</p>
         </div>';
    }
}

