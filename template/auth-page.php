<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Best eCommerce platform for multi-store retailers. Free trial."/>
    <meta name="robots" content="index, follow"/>
    <meta name="description"
          content="Online stores that help merchants grow sales and operations across online and retail channels."/>
    <meta name="keywords"
          content="e commerce,eCommerce,web store,online retail,merchant,inventory,cost of goods sold,shipping,order processing,bundles,pickup in-store, fulfillment"/>
    <meta name="language" content="en"/>
    <title>Best eCommerce platform for multi-store retailers</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link href='https://fonts.googleapis.com/css?family=Cabin:400,600|Open+Sans:300,600,400' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/animate.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/icons.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/landing.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/responsive.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/blue.css"/>
    <style type="text/css">
        .standard-button,
        .navbar-register-button {
            filter: none;
        }

        .container1 {
            /*display: block;*/
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            /*font-size: 22px;*/
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container1 input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container1:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container1 input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container1 input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container1 .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>
</head>
<body>
<header class="header" data-stellar-background-ratio="0.5" id="home">
    <div class="overlay-layer">
        <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="" href="#">
<!--                        <img src="--><?php //echo plugins_url('/img/w3bstore-logo.jpg', __FILE__); ?><!--">-->
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-6 align-self-center">
                    <div class="ard card-block">
                        <h4 class="dark-text form-heading">
                            <img src="<?php echo plugins_url('/img/shop-icon.png', __FILE__); ?>" style="width: 10%">
                            <br>
                           Sign In to W3bstore
                        </h4>
                        <div style="left: 0px; opacity: 1;  top: 0px; width: 100%; display: block;"
                             class="">
                            <?php
//                            if (isset($_GET['status']) && $_GET['status'] == "success") {
//                                echo '<div class="notice notice-success is-dismissible">
//                         <p style="text-align: left">'. $_GET['msg'].'</p>
//         </div>';
//                            } else if (isset($_GET['status']) && $_GET['status'] == "error") {
//echo '<div class="notice notice-error is-dismissible">
//                         <p style="text-align: left">'. $_GET['msg'].'</p>
//         </div>';
//                            }
                            ?>
                            <form class="align-self-center"
                                  action="<?php get_admin_url() ?>admin-post.php"
                                  method="post"
                                  style="display: inline-block;">
                                <?php
                                ?>
                                <input
                                        type="email" id="email" name="email" value="owner@w3bstore.com" class="form-control input-box"
                                        placeholder="Enter User name (Email Address)">

                                <input
                                        type="password" id="password" value="demodemo" name="password" class="form-control input-box"
                                        placeholder="Enter W3bstore password">

                                <label class="container1" style="font-weight: normal">
                                    New to W3bstore? Create a free account
                                    <input type="checkbox" name="new_account" value="1">
                                    <span class="checkmark"></span>
                                </label>
                                <br>
                                <br>
                                <input type="hidden" name="action" value="auth">
                                <button class="btn btn-primary standard-button">SIGN IN</button>
                            </form>
                        </div>
                        <p style="margin-top: 20px">
                            W3bstore hosts your eCommerce and retail Point of Sale on a Unified <br> Commerce platform
                            for seamless omnichannel
                            selling.
                        </p>
                    </div>
                </div>
            </div>
        </div>
</header>
</body>
</html>
