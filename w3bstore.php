<?php
/**
 * Plugin Name: W3bStore
 * Plugin URI: https://w3bstore.com/
 * Description: Online stores that help merchants grow sales and operations across online and retail channels.
 * Version: 1.0
 * Author: Salih Mohamed
 * Author URI: https://w3bstore.com/
 **/
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "w3b_store_class.php";
global $errors, $success;
$errors = 0;
$success = 0;
add_action('admin_menu', 'extra_post_info_menu');
add_action('after_setup_theme', 'w3bstore_functions');
register_activation_hook(__FILE__, 'w3bstore_plugin_activate');
register_deactivation_hook(__FILE__, 'w3bstore_plugin_deactivate');
//add_action('admin_post_auth-submit', '_handle_form_action');
function extra_post_info_menu()
{
    $active_menus = w3b_store_active_menus();
    $page_title = 'W3bstore Sales Funnel';
    $menu_title = 'W3bstore';
    $capability = 'manage_options';
    $menu_slug = '';
    $function = 'get_administrator_email';
    $icon_url = 'dashicons-media-code';
    $position = 4;
    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);
    foreach ($active_menus as $active_menu) {
        add_submenu_page($menu_slug, $menu_title, ucfirst($active_menu), $capability, strtolower($active_menu), "w3b_store_" . $active_menu);
    }
    add_submenu_page($menu_slug, $menu_title, "Setup", $capability, "setup", "w3b_store_show_setup_frm");
}

function w3b_store_dashboard()
{
    $obj = new w3bStoreClass();
    $response = $obj->w3bstoreLogin();
    $response = json_decode($response, true);
    if (isset($response['status']) && $response['status'] == "success") {
        echo '<script>window.location.href = "' . $response['url'] . '";</script>';
    }
}

function w3b_store_show_setup_frm()
{
    global $errors, $success;
    $obj = new w3bStoreClass();
    $auth = $obj->w3bstore_user_stored_token();
    if (empty($auth)) {
        include('template/auth-page.php');
    } else {
        include('template/auth-revoke-page.php');
    }
}

function w3b_store_active_menus()
{
    global $wpdb;
    $table = $wpdb->prefix . "w3bstore_menus";
    $menus = $wpdb->get_results("SELECT * FROM $table  ORDER BY bar_name DESC");
    $nMenu = [];
    foreach ($menus as $menu) {
        $nMenu[$menu->icon] = strtolower($menu->bar_name);
    }
    return $nMenu;
}

function w3bstore_plugin_activate()
{
    $obj = new w3bStoreClass();
    $obj->w3bstore_install_tables();
}

function w3bstore_plugin_deactivate()
{
    $obj = new w3bStoreClass();
    $obj->w3bstore_uninstall_tables();
}

function w3bstore_functions()
{
    global $errors;
    include('w3b_store_functions.php');
}


function get_administrator_email()
{

}

